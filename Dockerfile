# hadolint ignore=DL3006
ARG IMAGE_FROM_SHA
# hadolint ignore=DL3007
FROM jfxs/ci-toolkit:latest as ci-toolkit

# hadolint ignore=DL3006
FROM ${IMAGE_FROM_SHA}

ARG IMAGE_FROM_SHA
ARG NGINX_VERSION
ARG BUILD_DATE
ARG VCS_REF="DEV"

ENV container docker

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="hello-world" \
    org.opencontainers.image.description="A lightweight automatically updated and tested Hello world docker image" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="GPL-3.0-or-later" \
    org.opencontainers.image.version="${NGINX_VERSION}" \
    org.opencontainers.image.url="https://hub.docker.com/r/jfxs/hello-world" \
    org.opencontainers.image.source="https://gitlab.com/fxs/hello-world" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

RUN mkdir -p /hello && rm -f /etc/nginx/conf.d/default.conf
COPY files/default.conf /etc/nginx/conf.d/default.conf
COPY files/index.html /hello
COPY files/uid-entrypoint.sh /usr/local/bin/
COPY --from=ci-toolkit /usr/local/bin/get-local-versions.sh /usr/local/bin/get-local-versions.sh

RUN sed -i 's/^user/#user/' /etc/nginx/nginx.conf \
    && sed -i '/^http/a \ \  \ server_tokens off;' /etc/nginx/nginx.conf \
    && chgrp -R 0 /var/cache/nginx /var/log/nginx /var/run \
    && chmod -R g=u /var/cache/nginx /var/log/nginx /var/run \
    && chgrp 0 /etc/nginx/conf.d /hello \
    && chmod g=u /etc/passwd /etc/nginx/conf.d/default.conf /hello/index.html /etc/nginx/conf.d /hello \
    && /usr/local/bin/get-local-versions.sh -f ${IMAGE_FROM_SHA} -s nginx=${NGINX_VERSION}

ENTRYPOINT ["uid-entrypoint.sh"]

HEALTHCHECK --interval=1m --timeout=3s \
  CMD wget -O /dev/null http://localhost:8080 || exit 1

USER 10010
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
